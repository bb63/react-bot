const Discord = require('discord.js');
const settings = require('./settings.json');
const bot = new Discord.Client({disableEveryone: true});
bot.on('ready', async () => {
  console.log(`${bot.user.username} is Reporting for duty!`);
  bot.user.setActivity(`..`);
  try {
      let link = await bot.generateInvite(["ADMINISTRATOR"]);
      console.log(link);
  } catch(e) {
      console.log(e.stack);
  }
  var channel = bot.channels.get("447534669467222046");
  channel.bulkDelete(2);
  channel.send({
    "embed": {
      "color": 11976479,
      "fields": [
        {
          "name": "Discord Role Assignment",
          "value": "React with each of the platforms you play on by clicking on the corresponding platform icon below."
        }
      ]
    }
  })
  .then(msg => { 
    msg.react('447536397478264842');
    msg.react('447536396161253376');
    msg.react('447536396119441408');
    msg.react('447536398761852948');
  });

});

bot.on('messageReactionAdd', async (messageReaction, user) => {
    let gaming = await messageReaction.emoji.guild.roles.find("name", "Gamers");
    let pc = await messageReaction.emoji.guild.roles.find("name", "PC");
    let xbox = await messageReaction.emoji.guild.roles.find("name", "XBOX");
    let ps4 = await messageReaction.emoji.guild.roles.find("name", "PS4");

    var guildUser = await messageReaction.emoji.guild.fetchMember(user);
    if(user.bot) {
        return;
    }
    if(messageReaction.emoji.id == "447536397478264842") {
        console.log("xbox");
        if(guildUser.roles.has(xbox.id)) {
            console.log("has role");
            return;
        }
        messageReaction.emoji.guild.member(guildUser.user).addRole(xbox);

    }else if(messageReaction.emoji.id == "447536396161253376") {
        console.log("pc");
        if(guildUser.roles.has(pc.id)) {
            console.log("has role");
            return;
        }
        messageReaction.emoji.guild.member(guildUser.user).addRole(pc);
    }else if(messageReaction.emoji.id == "447536396119441408") {
        console.log("ps4");
        if(guildUser.roles.has(ps4.id)) {
            console.log("has role");
            return;
        }
        messageReaction.emoji.guild.member(guildUser.user).addRole(ps4);

    }else if(messageReaction.emoji.id == "447536398761852948") {
        console.log("gaming");
        if(guildUser.roles.has(gaming.id) || guildUser.roles.has(gaming.id)) {
            console.log("has role");
            return;
        }
        messageReaction.emoji.guild.member(guildUser.user).addRole(gaming);

    }
});

bot.on('messageReactionRemove', async (messageReaction, user) => {
    let pc = await messageReaction.emoji.guild.roles.find("name", "PC");
    let xbox = await messageReaction.emoji.guild.roles.find("name", "XBOX");
    let ps4 = await messageReaction.emoji.guild.roles.find("name", "PS4");

    var guildUser = await messageReaction.emoji.guild.fetchMember(user);

    if(user.bot) {
        return;
    }
    if(messageReaction.emoji.id == "447536397478264842") {
        messageReaction.emoji.guild.member(guildUser.user).removeRole(xbox);
    }else if(messageReaction.emoji.id == "447536396161253376") {
        messageReaction.emoji.guild.member(guildUser.user).removeRole(pc);
    }else if(messageReaction.emoji.id == "447536396119441408") {
        messageReaction.emoji.guild.member(guildUser.user).removeRole(ps4);
    }
})

bot.on('error', error => {
    return;
})
// login bot with token
bot.login(settings.secret.botToken);